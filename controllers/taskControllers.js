const Task = require('./../models/Tasks.js');

module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	});

	return newTask.save().then(result => result);
}

module.exports.getTask = (reqBody) => {

	return Task.findOne({name: reqBody.name}).then(result => result);
}

module.exports.getSpecificTask = (reqParams) => {

	return Task.findById({_id: reqParams.id}).then(result => result);
}

module.exports.changeStatusTask = (reqBody) => {

	return Task.findOneAndUpdate({name: reqBody.name}, {status: "complete"}).then(result => result);
}

module.exports.updateTask = (reqParams) => {

	return Task.findByIdAndUpdate({_id: reqParams.id}, {status: "complete"}).then(result => result);
}