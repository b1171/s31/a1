
//to query or manipulate database, import the model module to use the model methods
const User = require('./../models/User');
	//find()  ---> array of documents []
	//findOne()
	//findOneAnd..
	//findByIdAnd..

//userController is a module for business logic/ API tasks to be done
module.exports.getAllUsers = () => {
	//function has to do the task

	//first, find the matching document/s(single or array of documents)
		//manipulating database returns a promise
		//.then() handles the promise
			//resolve
			//reject
	return User.find().then( (result) => {
		// console.log(result); //array of documents

		return result

	//return the  matching document

	})
}


module.exports.register = (reqBody) => {

	//find the matching document using model method
	return User.findOne({email: reqBody.email}).then( (result) => {
		console.log(result)

		//if result != null, return false/`user exist`
		//if result == null, save the user
		if(result != null){
			return `User already exists`
		} else {

			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				mobileNo: reqBody.mobileNo,
				password: reqBody.password
			})

			return newUser.save().then( (result) => `User saved!`)
		}
	})
}

module.exports.updateUser = (reqBody) => {
	return User.findOneAndUpdate({email: reqBody.email}, {isAdmin: true}).then(result => `User updated!`)
}

module.exports.deleteUser = (reqBody) => {
	return User.findOneAndDelete({email: reqBody.email}).then(result => `User deleted`)
}

module.exports.getSpecificUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => result);
}

module.exports.getById = (req) => {
	return User.findById({_id: req.params.id}).then(result => result);
}